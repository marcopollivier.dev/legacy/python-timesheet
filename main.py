from datetime import datetime

#timesheet.py
#http://pythonclub.com.br/introducao-classes-metodos-python-basico.html
class TimeSheet(object):

    #TimeSheet('31/10/2016 09:00', '31/10/2016 12:00', '31/10/2016 13:00', '31/10/2016 17:00')
    def __init__(self, entrada, idaAlmoco, voltaAlmoco, saida):
        self.entrada = entrada
        self.idaAlmoco = idaAlmoco
        self.voltaAlmoco = voltaAlmoco
        self.saida = saida

    def calcula_dia(self):
        date1 = int(datetime.strptime(self.entrada, '%d/%m/%Y %H:%M').strftime("%s"))
        date2 = int(datetime.strptime(self.idaAlmoco, '%d/%m/%Y %H:%M').strftime("%s"))
        date3 = int(datetime.strptime(self.voltaAlmoco, '%d/%m/%Y %H:%M').strftime("%s"))
        date4 = int(datetime.strptime(self.saida, '%d/%m/%Y %H:%M').strftime("%s"))

        difdate = ((date2 - date1) + (date4 - date3))
        difdate = (difdate/60) - 480

        return difdate
